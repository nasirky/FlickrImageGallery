//
//  PublicServiceUrls.swift
//  FlickrFetcherSDK
//
//  Created by Ghulam Nasir.
//  Copyright © 2018 Ghulam Nasir. All rights reserved.
//

import Foundation
class PublicServiceUrls {
    /// The url for accessing Flickr's public photos feed.
    static let publicPhotos = Urls.feeds.appending("photos_public.gne")
}
