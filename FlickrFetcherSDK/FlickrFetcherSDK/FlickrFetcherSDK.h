//
//  FlickrFetcherSDK.h
//  FlickrFetcherSDK
//
//  Created by Ghulam Nasir.
//  Copyright © 2018 Ghulam Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for FlickrFetcherSDK.
FOUNDATION_EXPORT double FlickrFetcherSDKVersionNumber;

//! Project version string for FlickrFetcherSDK.
FOUNDATION_EXPORT const unsigned char FlickrFetcherSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FlickrFetcherSDK/PublicHeader.h>


